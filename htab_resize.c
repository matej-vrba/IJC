#include "htab.h"
#include "htab_impl.h"
#include <stdlib.h>



void htab_resize(htab_t *t, size_t newn){
  if(newn == 0) return;
  
  //if htab is expanding, realloc must be called first
  if(t->arr_size < newn){
    struct htab_item* (*new_arr)[] = realloc(t->arr,
                                        newn * sizeof(struct htab_item*));
    if(new_arr == NULL) return;
    for(size_t i = t->arr_size; i < newn; i++)
      (*new_arr)[i] = NULL;
    t->arr = new_arr;
  }
  
  for(size_t i = 0; i < t->arr_size; i++){
    struct htab_item* item = (*t->arr)[i];
    if(item == NULL) continue;
    while((item = (*t->arr)[i]) != NULL){//process first element
      size_t index = htab_hash_function(item->pair.key) % newn;
      if(index == i) break;// index didn't change
      (*t->arr)[i] = item->next;
      item->next = (*t->arr)[index];
      (*t->arr)[index] = item;
    }
    if(item == NULL) continue;

    struct htab_item* prev = item;
    item = item->next;
    while(item != NULL){
      size_t index = htab_hash_function(item->pair.key) % newn;
      if(index == i){
        item = item->next;
        continue;
      }
      prev->next = item->next;
      item->next = (*t->arr)[index];
      (*t->arr)[index] = item;
      item = prev->next;
    }
  }
  
  //if htab is shrinking, realloc must be called last
  if(t->arr_size != newn){
    struct htab_item* (*new_arr)[] = realloc(t->arr,
                                        newn * sizeof(struct htab_item*));
    if(new_arr == NULL) {
      size_t old_size = t->arr_size;
      t->arr_size = newn;
      htab_resize(t, old_size); //undo any changes
      return;
    }
    t->arr = new_arr;
  }
    t->arr_size = newn;

}
