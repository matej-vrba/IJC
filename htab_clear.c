#include "htab.h"
#include "htab_impl.h"
#include <stdlib.h>

static void free_item_list(struct htab_item* item){
  if(item->next != NULL)
    free_item_list(item->next);
  free((void*)item->pair.key);
  free(item);
}

void htab_clear(htab_t*t){
  if(t == NULL)return;
  
  for(size_t i = 0; i < t->arr_size; i++){
    struct htab_item* item = (*t->arr)[i];
    if(item != NULL){
      free_item_list(item);
      (*t->arr)[i] = NULL;
    }
  }
  t->size = 0;
}
