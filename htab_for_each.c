#include "htab.h"
#include "htab_impl.h"
#include <stdlib.h>


void htab_for_each(const htab_t * t, void (*f)(htab_pair_t *data)){
  if(t == NULL || f == NULL) return;
  
  for(size_t i = 0; i < t->arr_size; i++){
    struct htab_item *item = (*t->arr)[i];
    while(item != NULL){
    //to prevent key changes
      f(&item->pair);
      item = item->next;
    }
  }
  
}
