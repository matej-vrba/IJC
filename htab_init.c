#include "htab.h"
#include "htab_impl.h"
#include <stdlib.h>

htab_t *htab_init(size_t n) {
  htab_t *htab = malloc(sizeof(htab_t) + sizeof(struct htab_item*));
  if (htab == NULL) return NULL;
	htab->arr = malloc(sizeof(struct htab_item) * n);
	if(htab->arr == NULL) return NULL;
  htab->arr_size = n;
	htab->size = 0;

	for(size_t i = 0; i < n; i++)
			(*htab->arr)[i] = NULL;

	return htab;
}
