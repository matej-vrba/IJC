#include "htab.h"
#include "htab_impl.h"
#include <stdlib.h>

static int resize_if_needed(htab_t* t){
  double avg_len = 0;
  for(size_t i = 0; i < t->arr_size; i++){
    size_t size = 0;
    struct htab_item* item = (*t->arr)[i];
    while(item != NULL){
      size++;
      item = item->next;
    }
    avg_len += (double)size / t->arr_size;
  }
  if(avg_len > AVG_LEN_MAX)
    htab_resize(t, t->arr_size * 2);

  if(avg_len < AVG_LEN_MIN && t->arr_size > 1)
    htab_resize(t, t->arr_size / 2);
return 0;
}

htab_pair_t * htab_lookup_add(htab_t * t, htab_key_t key){
  if(t == NULL) return NULL;
  size_t index = htab_hash_function(key) % t->arr_size;
  
  struct htab_item* item = (*t->arr)[index];
  //try to find key in list
  while(item != NULL){
    if(strcmp(item->pair.key, key) == 0) return &item->pair;
    item = item->next;
  }
  //key is not in list
  //copy key
  char* new_item_key = malloc(strlen(key) + 1);
  if(new_item_key == NULL) return NULL;
  strcpy(new_item_key, key);

  struct htab_item* new = malloc(sizeof(struct htab_item));
  if(new == NULL) return NULL;
  new->pair.key = new_item_key;// assing key to new item
  new->pair.value = 0;
  //insert new key at beginning
  new->next = (*t->arr)[index];
  (*t->arr)[index] = new;
  t->size++;

  resize_if_needed(t);
  return &new->pair;
}
