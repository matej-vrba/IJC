#ifndef __HTAB_IMPL_H__
#define __HTAB_IMPL_H__

#include "htab.h"

struct htab{
		size_t size;//number of htab_item
		size_t arr_size;//size of arr[]
    struct htab_item *arr[];
};

struct htab_item {
		htab_pair_t pair;
    struct htab_item *next;
};

#endif //__HTAB_IMPL_H__
