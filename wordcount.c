#include "htab.h"
#include "io.h"
#include <stdlib.h>

static void tab_print(htab_pair_t* pair){
  printf("%s: %d\n", pair->key, pair->value);
}

int main(int argc, char** argv){
  htab_t* tab = htab_init(1);
  if(tab == NULL){
    fprintf(stderr, "Failed to initialize");
    return 1;
  }

  char buff[512];
  while(read_word(buff, sizeof(buff), stdin) != EOF){
    htab_lookup_add(tab, buff)->value++;
  }
  htab_for_each(tab, tab_print);
  htab_free(tab);
}
