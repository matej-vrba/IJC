#include "htab.h"
#include "htab_impl.h"
#include <stdlib.h>


void htab_free(htab_t* tab){
  if(tab == NULL) return;
  htab_clear(tab);
  free(tab->arr);
  free(tab);
}
