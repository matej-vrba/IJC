#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int process_flag(char **argv) {
  char *exe_name = argv[0];
  int line_num = 10;

  while (*(++argv) != NULL) {
    if (strncmp(argv[0], "-n", 2) != 0)//check if argument begins with -n
      continue;
    //check if number is part of argument (-n10) or as separate argument (-n 10)
    if (strcmp(argv[0], "-n") != 0) { //< line num is part of argument (-n10)
      char *end;
      line_num = strtol(argv[0] + 2, &end, 10);
      if (end[0] != '\0') {
        fprintf(stderr, "%s invalid number of lines: ‘%s’\n", exe_name,
                argv[0] + 2);
        return -1;
      }
      argv[0] = NULL;
    } else { //< line num is separate argument (-n 10)
      argv[0] = NULL;
      argv++;
      char *end;
      if (argv[0] == NULL) {
        fprintf(stderr, "%s: -n requires an argument\n", exe_name);
        return -1;
      }
      line_num = strtol(argv[0], &end, 10);
      if (end[0] != '\0') {
        fprintf(stderr, "%s invalid number of lines: ‘%s’\n", exe_name,
                argv[0]);
        return -1;
      }
      argv[0] = NULL;
    }
  }
  return line_num;
}

int count_filenames(int argc, char **argv) {
  int num = 0;
  for (int i = 1; i < argc; i++)
    if (argv[i] != NULL) // check for removed -n flag
      num++;
  return num;
}

int read_line(char **line_buff, size_t *n, FILE *file) {
  int ch;
  *n = 0;

  while((ch = fgetc(file)) != EOF){
    if(*n %64 == 0){
      char* nbuff = realloc(*line_buff, *n + 64);
      if(nbuff == NULL)return -1;
      *line_buff = nbuff;
    }
    (*line_buff)[(*n)++] = ch;
    if(ch == '\n')
      break;
  }
  if(*n == 0) return EOF;
  (*line_buff)[*n] = '\0';
  return strlen(*line_buff);
}

//prints last x lines from file
// filename - file to print
// num_lines - how many lines to print
// returns 0 on failure, 1 otherwise
int read_file(char *filename, int num_lines) {
  FILE *f = fopen(filename, "r");
  if (f == NULL){
    perror(filename);
    return 0;
  }

  int newline_count = 0;

  if (fseek(f, 0, SEEK_END)){
    perror(filename);
    goto exit_error;
  }

  unsigned long long pos = ftell(f);

  while (pos) {
    if(fseek(f, --pos, SEEK_SET)){
      perror(filename);
      goto exit_error;
    }
    if (getc(f) == '\n')
      if (newline_count++ == num_lines)
        pos = 0;
  }
  char *line = NULL;
  size_t buff_len = 0;

  while (read_line(&line, &buff_len, f) != EOF)
    printf("%s", line);

  return 1;
exit_error:
  fclose(f);
  return 0;
}

int main(int argc, char **argv) {
  int num_lines = process_flag(argv);
  if (num_lines == -1) return 1; //failed to read number of lines
  if (num_lines == 0) return 0; // -n 0 - nothing to do
  int num_of_files = count_filenames(argc, argv);//count files excluding -n argument

  size_t buff_len = 0;
  char *line = NULL;
  int line_len = 0;
  int processed_files = 0;

  // 1 - skip executable name
  for (int i = 1; i < argc; i++) {
    if (argv[i] == NULL) //< flags were replaces by NULL
      continue;

    // check if filename is dir
    struct stat path_stat;
    stat(argv[i], &path_stat);
    if (S_ISDIR(path_stat.st_mode)) {
      fprintf(stderr, "%s: error reading '%s': Is a directory\n", argv[0],
              argv[i]);
    }

    //open file
    FILE *file = fopen(argv[i], "r");
    if (file == NULL) {
      fprintf(stderr, "%s: cannot open '%s' for reading\n", argv[0], argv[i]);
      processed_files = 1;
      continue;
    }
    //output separator when printing multiple files
    if (num_of_files > 1)
      printf("==> %s <==\n", argv[i]);

    // break of reading failed, error is aleready printed
    if(!read_file(argv[i], num_lines)) break;
    processed_files = 1;
    fclose(file);
  }

  char **lines;
  lines = malloc(num_lines);
  for(int i = 0; i < num_lines; i++)
    lines[i] = NULL;

  int index  = 0;
  if (processed_files == 0) { //< no file as argument, process stdin
    while ((line_len = read_line(&lines[index++], &buff_len, stdin)) != EOF) {
      index %= num_lines;
    }
    for(int i = 0; i < num_lines; i++)
      printf("%s", lines[i]);
  }
  free(line);
  return 0;
}
