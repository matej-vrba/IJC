#include "htab.h"
#include "htab_impl.h"
#include <stdlib.h>

bool htab_erase(htab_t * t, htab_key_t key){
  if(t == NULL || key == NULL) return false;
  size_t index = htab_hash_function(key) % t->arr_size;
  
  struct htab_item* item = (*t->arr)[index], * previous = NULL;
  
  if(strcmp(item->pair.key, key) == 0){
    (*t->arr)[index] = item->next;
    t->arr_size--;
    free((void*)item->pair.key);
    free(item);
    return true;
  }
  previous = item;
  item = item->next;
    
  while(item != NULL){
    if(strcmp(item->pair.key, key) == 0){
      previous->next = item->next;
      t->arr_size--;
      free((void*)item->pair.key);      
      free(item);
      return true;
    }
  }
  return false;
}
