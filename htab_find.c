#include "htab.h"
#include "htab_impl.h"

htab_pair_t * htab_find(htab_t * t, htab_key_t key){
  if(t == NULL || key == NULL) return NULL;
  size_t index = htab_hash_function(key) % t->arr_size;
  
  struct htab_item* item = (*t->arr)[index];
  //try to find key in list
  while(item != NULL){
    if(strcmp(item->pair.key, key) == 0) return &item->pair;
    item = item->next;
  }
  
  return NULL;
}
