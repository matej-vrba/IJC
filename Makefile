######################################
# Makefile
# IJC DU 1
# Autor: Matej Vrba <xvrbam03@stud.fit.vutbr.cz>
# Pro preklad jsem pouzival gcc (11.2.0) i clang (13.0.1)
######################################
CC=clang
CFLAGS += -g -std=c11 -pedantic -Wall -Wextra -O2 -march=native
#CFLAGS += -fsanitize=address
#LDFLAGS += -fsanitize=address

HTAB_SRCS =  $(wildcard htab*.c)
HTAB_OBJS  = $(patsubst %.c,%.o,$(HTAB_SRCS))

#source files
sources = $(wildcard *.c)
# compiler gneerated dependency files
deps = $(patsubst %.c,%.d,$(sources))

.PHONY: all clean run

all: tail wordcount wordcount-dynamic

run:


# || true so it removes obj_inline even if previous rm fails
clean:
	rm *.o *.d tail || true
	rm libhtab.{a,so} wordcount wordcount-dynamic sandbox  || true

wordcount: wordcount.o io.o libhtab.a
	$(CC) $(LDFLAGS) wordcount.o io.o libhtab.a -o $@

wordcount-dynamic: wordcount.o io.o libhtab.so
	$(CC) $(LDFLAGS) wordcount.o io.o -L. -lhtab -o $@

tail: tail.o
	$(CC) $(LDFLAGS) $< -o $@

libhtab.a: $(HTAB_OBJS)
	ar -crs $@ $^

libhtab.so: $(HTAB_OBJS)
	$(CC) -shared $^ -o $@

sandbox: $(HTAB_OBJS) sandbox.o 
	$(CC) $(LDFLAGS) $^ -o $@

#include dependency files
-include $(deps)
# generate .d file containing dependencies
%.o: %.c
	$(CC) $(CFLAGS) -MMD -MP -c $< -o $@
