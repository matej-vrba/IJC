#include <ctype.h>
#include "io.h"

int read_word(char *s, int max, FILE *f){
  int ch;
  int read = 0;
  while(isspace(ch = fgetc(f)));//read all spaces from input file
  ungetc(ch, f);//last read char wasn't space, push it back
  while((ch = fgetc(f)) != EOF && ! isspace(ch)){
    if(read < max - 1)
      s[read++] = ch;
  }
  s[read] = '\0';
  if(ch == EOF) return EOF;
  
  return read;
}
